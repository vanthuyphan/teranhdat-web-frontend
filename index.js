var express = require('express');
var app = express();

var expressLess = require('express-less');

app.use('/public/less', expressLess(__dirname + '/public/less'));
app.use('/public', express.static(__dirname + '/public'));

app.get('/', function(req, res){
  // res.send('hello world');
  res.redirect('/public/html/pages.html');
});


const PORT = 6060;

app.listen(PORT, function (err) {
	if (err) {
		throw err;
	}
	console.log("web address: http://localhost:" + PORT);
});
